#ifndef DV_PROCESSING_EXTERNAL_FMT_COMPAT_HPP
#define DV_PROCESSING_EXTERNAL_FMT_COMPAT_HPP

#include <fmt/chrono.h>
#include <fmt/compile.h>
#include <fmt/format.h>

#if defined(FMT_VERSION) && FMT_VERSION >= 80000

#	define DV_EXT_FMT_RUNTIME(VAR) fmt::runtime((VAR))
#	define DV_EXT_FMT_CONST        const

#else

#	define DV_EXT_FMT_RUNTIME(VAR) (VAR)
#	define DV_EXT_FMT_CONST

#endif

#endif // DV_PROCESSING_EXTERNAL_FMT_COMPAT_HPP

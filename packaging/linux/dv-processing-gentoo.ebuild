# Copyright 2021 iniVation AG
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit eutils cmake-utils

DESCRIPTION="Generic algorithms for event cameras."
HOMEPAGE="https://gitlab.com/inivation/dv/${PN}/"

SRC_URI="https://release.inivation.com/processing/${P}.tar.gz"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="amd64 x86 arm64 arm"
IUSE="debug test python"

RDEPEND=">=dev-libs/boost-1.74.0
	>=media-libs/opencv-4.2.0
	>=dev-cpp/eigen-3.3.9
	>=dev-libs/libcaer-3.3.12
	>=app-arch/lz4-1.9.0
	>=app-arch/zstd-1.5.0
	>=dev-libs/libfmt-7.1.3
	python? (
		>=dev-lang/python-3.8.0
		dev-python/numpy
	)"

DEPEND="${RDEPEND}
	virtual/pkgconfig
	>=dev-util/cmake-3.16.0"

src_configure() {
	local mycmakeargs=(
		-DENABLE_TESTS="$(usex test 1 0)"
		-DENABLE_PYTHON="$(usex python 1 0)"
		-DENABLE_BENCHMARKS=0
		-DENABLE_SAMPLES=0
	)

	cmake-utils_src_configure
}

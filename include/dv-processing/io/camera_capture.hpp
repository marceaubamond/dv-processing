#pragma once

#include "../core/core.hpp"
#include "../core/utils.hpp"
#include "../data/event_base.hpp"
#include "../data/frame_base.hpp"
#include "../data/imu_base.hpp"
#include "../data/trigger_base.hpp"
#include "../exception/exception.hpp"
#include "data_read_handler.hpp"
#include "discovery.hpp"

#include <boost/lockfree/spsc_queue.hpp>
#include <opencv2/imgproc.hpp>

#include <atomic>
#include <functional>
#include <future>
#include <thread>

namespace dv::io {

class StereoCapture;

class CameraCapture {
public:
	enum class BiasSensitivity {
		VeryLow  = DVX_DVS_CHIP_BIAS_SIMPLE_VERY_LOW,
		Low      = DVX_DVS_CHIP_BIAS_SIMPLE_LOW,
		Default  = DVX_DVS_CHIP_BIAS_SIMPLE_DEFAULT,
		High     = DVX_DVS_CHIP_BIAS_SIMPLE_HIGH,
		VeryHigh = DVX_DVS_CHIP_BIAS_SIMPLE_VERY_HIGH
	};

	enum class DavisReadoutMode { EventsAndFrames, EventsOnly, FramesOnly };

	enum class DavisColorMode {
		// Force grascale
		Grayscale = caer_davis_aps_frame_modes::APS_FRAME_GRAYSCALE,
		// Resort to default: color for color, grayscale to mono
		Color = caer_davis_aps_frame_modes::APS_FRAME_DEFAULT
	};

	enum class CameraType { Any, DAVIS, DVS };

private:
	std::atomic<bool> keepRunning;
	std::atomic<bool> waitForReset;

	struct caer_device_discovery_result discoveryResult {};

	int64_t timestampOffset;

	// The device handle
	std::unique_ptr<libcaer::devices::device> device = nullptr;

	using EventPacketBuffer
		= boost::lockfree::spsc_queue<std::pair<size_t, std::shared_ptr<libcaer::events::EventPacket>>>;

	struct SortedPacketBuffers {
		size_t packetCount = 0;

		EventPacketBuffer events   = EventPacketBuffer(10000);
		EventPacketBuffer imu      = EventPacketBuffer(10000);
		EventPacketBuffer triggers = EventPacketBuffer(10000);
		EventPacketBuffer frames   = EventPacketBuffer(1000);

		void acceptPacket(const std::shared_ptr<libcaer::events::EventPacket> &packet) {
			const auto eventType = static_cast<caer_default_event_types>(packet->getEventType());
			switch (eventType) {
				case SPECIAL_EVENT:
					triggers.push(std::make_pair(packetCount++, packet));
					break;
				case POLARITY_EVENT:
					events.push(std::make_pair(packetCount++, packet));
					break;
				case FRAME_EVENT:
					frames.push(std::make_pair(packetCount++, packet));
					break;
				case IMU6_EVENT:
					imu.push(std::make_pair(packetCount++, packet));
					break;
				default:
					break;
			}
		}

		void clearBuffers() {
			events.reset();
			imu.reset();
			triggers.reset();
			frames.reset();
		}
	};

	static float boschAccRateToFreq(const uint32_t value) {
		switch (value) {
			case BOSCH_ACCEL_12_5HZ:
				return 12.5f;
			case BOSCH_ACCEL_25HZ:
				return 25.f;
			case BOSCH_ACCEL_50HZ:
				return 50.f;
			case BOSCH_ACCEL_100HZ:
				return 100.f;
			case BOSCH_ACCEL_200HZ:
				return 200.f;
			case BOSCH_ACCEL_400HZ:
				return 400.f;
			case BOSCH_ACCEL_800HZ:
				return 800.f;
			case BOSCH_ACCEL_1600HZ:
				return 1600.f;
			default:
				throw dv::exceptions::InvalidArgument<uint32_t>("Invalid accelerometer measurement rate value", value);
		}
	}

	static float boschGyroRateToFreq(const uint32_t value) {
		switch (value) {
			case BOSCH_GYRO_25HZ:
				return 25.f;
			case BOSCH_GYRO_50HZ:
				return 50.f;
			case BOSCH_GYRO_100HZ:
				return 100.f;
			case BOSCH_GYRO_200HZ:
				return 200.f;
			case BOSCH_GYRO_400HZ:
				return 400.f;
			case BOSCH_GYRO_800HZ:
				return 800.f;
			case BOSCH_GYRO_1600HZ:
				return 1600.f;
			case BOSCH_GYRO_3200HZ:
				return 3200.f;
			default:
				throw dv::exceptions::InvalidArgument<uint32_t>("Invalid gyroscope measurement rate value", value);
		}
	}

	SortedPacketBuffers buffers;

	void stopReadout() {
		if (keepRunning) {
			keepRunning = false;
		}
	}

	[[nodiscard]] dv::Frame convertFramePacket(const std::shared_ptr<libcaer::events::EventPacket> &packet) const {
		auto frame           = reinterpret_cast<caerFrameEventPacket>(packet->getHeaderPointer());
		caerFrameEvent event = caerFrameEventPacketGetEvent(frame, 0);

		uint16_t *image = caerFrameEventGetPixelArrayUnsafe(event);

		// get image metadata
		caer_frame_event_color_channels frameChannels = caerFrameEventGetChannelNumber(event);

		const int32_t frame_width  = caerFrameEventGetLengthX(event);
		const int32_t frame_height = caerFrameEventGetLengthY(event);

		int cvMatTypeTarget;
		switch (frameChannels) {
			case GRAYSCALE:
				cvMatTypeTarget = CV_8UC1;
				break;
			case RGB:
				cvMatTypeTarget = CV_8UC3;
				break;
			case RGBA:
				cvMatTypeTarget = CV_8UC4;
				break;
		}

		cv::Mat converted(frame_height, frame_width, cvMatTypeTarget);

		uint16_t *lastPixel  = image + caerFrameEventGetPixelsMaxIndex(event);
		uint8_t *targetPixel = converted.data;
		for (uint16_t *pixel = image; pixel <= lastPixel; pixel++, targetPixel++) {
			*targetPixel = (*pixel >> 8);
		}

		switch (frameChannels) {
			case RGB:
				cv::cvtColor(converted, converted, cv::COLOR_RGB2BGR);
				break;
			case RGBA:
				cv::cvtColor(converted, converted, cv::COLOR_RGBA2BGRA);
				break;
			default:
				break;
		}

		int64_t exposure = caerFrameEventGetExposureLength(event);
		auto posX        = caerFrameEventGetPositionX(event);
		auto posY        = caerFrameEventGetPositionY(event);

		return {timestampOffset + caerFrameEventGetTimestamp64(event, frame) - exposure, exposure,
			static_cast<int16_t>(posX), static_cast<int16_t>(posY), converted, dv::FrameSource::SENSOR};
	}

	[[nodiscard]] dv::cvector<dv::IMU> convertImuPacket(
		const std::shared_ptr<libcaer::events::EventPacket> &packet) const {
		dv::cvector<dv::IMU> output;
		const libcaer::events::IMU6EventPacket oldPacketIMU(
			const_cast<caerEventPacketHeader>(packet->getHeaderPointer()), false);

		output.reserve(static_cast<size_t>(oldPacketIMU.getEventValid()));

		for (const auto &evt : oldPacketIMU) {
			if (!evt.isValid()) {
				continue;
			}

			dv::IMU imu{};
			imu.timestamp      = timestampOffset + evt.getTimestamp64(oldPacketIMU);
			imu.temperature    = evt.getTemp();
			imu.accelerometerX = evt.getAccelX();
			imu.accelerometerY = evt.getAccelY();
			imu.accelerometerZ = evt.getAccelZ();
			imu.gyroscopeX     = evt.getGyroX();
			imu.gyroscopeY     = evt.getGyroY();
			imu.gyroscopeZ     = evt.getGyroZ();

			output.push_back(imu);
		}
		return output;
	}

	[[nodiscard]] dv::cvector<dv::Trigger> convertTriggerPacket(
		const std::shared_ptr<libcaer::events::EventPacket> &packet) const {
		const libcaer::events::SpecialEventPacket oldPacketSpecial(
			const_cast<caerEventPacketHeader>(packet->getHeaderPointer()), false);

		dv::cvector<dv::Trigger> output;
		output.reserve(static_cast<size_t>(oldPacketSpecial.getEventValid()));

		for (const auto &evt : oldPacketSpecial) {
			if (!evt.isValid()) {
				continue;
			}

			dv::Trigger trigger{};

			switch (static_cast<caer_special_event_types>(evt.getType())) {
				case TIMESTAMP_RESET:
					trigger.type = dv::TriggerType::TIMESTAMP_RESET;
					break;
				case EXTERNAL_INPUT_RISING_EDGE:
					trigger.type = dv::TriggerType::EXTERNAL_SIGNAL_RISING_EDGE;
					break;
				case EXTERNAL_INPUT_FALLING_EDGE:
					trigger.type = dv::TriggerType::EXTERNAL_SIGNAL_FALLING_EDGE;
					break;
				case EXTERNAL_INPUT_PULSE:
					trigger.type = dv::TriggerType::EXTERNAL_SIGNAL_PULSE;
					break;
				case EXTERNAL_GENERATOR_RISING_EDGE:
					trigger.type = dv::TriggerType::EXTERNAL_GENERATOR_RISING_EDGE;
					break;
				case EXTERNAL_GENERATOR_FALLING_EDGE:
					trigger.type = dv::TriggerType::EXTERNAL_GENERATOR_FALLING_EDGE;
					break;
				case APS_FRAME_START:
					trigger.type = dv::TriggerType::APS_FRAME_START;
					break;
				case APS_FRAME_END:
					trigger.type = dv::TriggerType::APS_FRAME_END;
					break;
				case APS_EXPOSURE_START:
					trigger.type = dv::TriggerType::APS_EXPOSURE_START;
					break;
				case APS_EXPOSURE_END:
					trigger.type = dv::TriggerType::APS_EXPOSURE_END;
					break;
				default:
					// Ignoring the packet
					continue;
			}

			trigger.timestamp = timestampOffset + evt.getTimestamp64(oldPacketSpecial);

			output.push_back(trigger);
		}
		return output;
	}

	[[nodiscard]] dv::EventStore convertEventsPacket(
		const std::shared_ptr<libcaer::events::EventPacket> &packet) const {
		dv::EventStore output;
		auto eventHeader        = reinterpret_cast<caerPolarityEventPacket>(packet->getHeaderPointer());
		const int32_t numEvents = eventHeader->packetHeader.eventNumber;
		for (int32_t i = 0; i < numEvents; i++) {
			caerPolarityEvent event = caerPolarityEventPacketGetEvent(eventHeader, i);
			output.emplace_back(timestampOffset + caerPolarityEventGetTimestamp64(event, eventHeader),
				caerPolarityEventGetX(event), caerPolarityEventGetY(event), caerPolarityEventGetPolarity(event));
		}
		return output;
	}

	static inline bool containsResetEvent(const std::shared_ptr<libcaer::events::EventPacket> &packet) {
		if (packet->getEventType() != SPECIAL_EVENT) {
			return false;
		}

		const libcaer::events::SpecialEventPacket oldPacketSpecial(
			const_cast<caerEventPacketHeader>(packet->getHeaderPointer()), false);

		return std::any_of(oldPacketSpecial.begin(), oldPacketSpecial.end(), [](const auto &evt) {
			return evt.isValid() && evt.getType() == TIMESTAMP_RESET;
		});
	}

	void discoverMatchingCamera(const std::string &cameraName, const CameraType type) {
		const auto allCameras = libcaer::devices::discover::all();
		auto iter             = std::find_if(
						allCameras.begin(), allCameras.end(), [&cameraName, &type](const caer_device_discovery_result &camera) {
                bool typeMatches
                    = type == CameraType::Any
                      || (type == CameraType::DVS
                          && (camera.deviceType == CAER_DEVICE_DVXPLORER || camera.deviceType == CAER_DEVICE_DVS128
                              || camera.deviceType == CAER_DEVICE_DVS132S))
                      || (type == CameraType::DAVIS && camera.deviceType == CAER_DEVICE_DAVIS);

                bool nameMatches = cameraName.empty() || cameraName == internal::getDiscoveredCameraName(camera);
                return nameMatches && typeMatches;
						});

		if (iter == allCameras.end()) {
			throw std::runtime_error("No compatible device discovered");
		}
		else {
			discoveryResult = *iter;
		}
	}

	void sendTimestampReset() {
		switch (discoveryResult.deviceType) {
			case CAER_DEVICE_DVXPLORER:
				deviceConfigSet(DVX_MUX, DVX_MUX_TIMESTAMP_RESET, true);
				break;
			case CAER_DEVICE_DVS132S:
				deviceConfigSet(DVS132S_CONFIG_MUX, DVS132S_CONFIG_MUX_TIMESTAMP_RESET, true);
				break;
			case CAER_DEVICE_DVS128:
				deviceConfigSet(DVS128_CONFIG_DVS, DVS128_CONFIG_DVS_TIMESTAMP_RESET, true);
				break;
			case CAER_DEVICE_DAVIS:
				deviceConfigSet(DAVIS_CONFIG_MUX, DAVIS_CONFIG_MUX_TIMESTAMP_RESET, true);
				break;
			default:
				throw dv::exceptions::RuntimeError("Device does not support timestamp reset");
		}
	}

	explicit CameraCapture(const std::string &cameraName, const CameraType type, bool waitForReset) :
		keepRunning(true), waitForReset(waitForReset) {
		discoverMatchingCamera(cameraName, type);

		device = libcaer::devices::discover::open(0, discoveryResult);
		device->sendDefaultConfig();

		enableDavisAutoExposure();
		setDVSGlobalHold(false);

		if (!waitForReset) {
			timestampOffset = dv::now();
		}
		else {
			timestampOffset = -1;
		}
		device->dataStart(
			[](void *ptr) {
				auto *self = reinterpret_cast<CameraCapture *>(ptr);
				if (self->keepRunning.load(std::memory_order_relaxed)) {
					// Read and sort packets
					if (auto packetContainer = self->device->dataGet(); packetContainer != nullptr) {
						for (auto &packet : *packetContainer) {
							if (packet == nullptr) {
								continue;
							}
							if (self->waitForReset.load(std::memory_order_relaxed)) {
								if (containsResetEvent(packet)) {
									self->waitForReset = false;
									if (self->timestampOffset < 0) {
										self->timestampOffset = dv::now();
									}
								}
							}
							else {
								dv::runtime_assert(
									self->timestampOffset > 0, "Receiving packets without valid timestamp offset");
								self->buffers.acceptPacket(packet);
							}
						}
					}
				}
			},
			nullptr, reinterpret_cast<void *>(this),
			[](void *ptr) {
				auto *self = reinterpret_cast<CameraCapture *>(ptr);
				self->stopReadout();
			},
			reinterpret_cast<void *>(this));
	}

	friend class dv::io::StereoCapture;

public:
	/**
	 * Create a camera capture class which opens first discovered camera of any type.
	 */
	CameraCapture() : CameraCapture("", CameraType::Any, false) {
	}

	/**
	 * Create a camera capture class which opens a camera according to given parameters.
	 * @param cameraName 	Camera name, an empty string will match any name.
	 * @param type 			Type of camera, one of: any, DVS, or DAVIS.
	 */
	explicit CameraCapture(const std::string &cameraName, const CameraType type = CameraType::Any) :
		CameraCapture(cameraName, type, false) {
	}

	/**
	 * Parse and retrieve next event batch.
	 * @return 		Event batch or `std::nullopt` if no events were received since last read.
	 */
	std::optional<dv::EventStore> getNextEventBatch() {
		if (buffers.events.empty()) {
			return std::nullopt;
		}

		dv::EventStore output;
		buffers.events.consume_one([this, &output](const auto &packet) {
			output = convertEventsPacket(packet.second);
		});
		return output;
	}

	/**
	 * Parse and retrieve next frame.
	 * @return 		Frame or `std::nullopt` if no frames were received since last read.
	 */
	std::optional<dv::Frame> getNextFrame() {
		if (buffers.frames.empty()) {
			return std::nullopt;
		}

		dv::Frame output;
		buffers.frames.consume_one([this, &output](const auto &packet) {
			output = convertFramePacket(packet.second);
		});
		return output;
	}

	/**
	 * Parse and retrieve next IMU data batch.
	 * @return 		IMU data batch or `std::nullopt` if no IMU data was received since last read.
	 */
	[[nodiscard]] std::optional<dv::cvector<dv::IMU>> getNextImuBatch() {
		if (buffers.imu.empty()) {
			return std::nullopt;
		}
		dv::cvector<dv::IMU> output;
		buffers.imu.consume_one([this, &output](const auto &packet) {
			output = convertImuPacket(packet.second);
		});
		return output;
	}

	/**
	 * Parse and retrieve next trigger data batch.
	 * @return 		Trigger data batch or `std::nullopt` if no triggers were received since last read.
	 */
	[[nodiscard]] std::optional<dv::cvector<dv::Trigger>> getNextTriggerBatch() {
		if (buffers.triggers.empty()) {
			return std::nullopt;
		}
		dv::cvector<dv::Trigger> output;
		buffers.triggers.consume_one([this, &output](const auto &packet) {
			output = convertTriggerPacket(packet.second);
		});
		return output;
	}

	/**
	 * Get event stream resolution.
	 * @return 		Event stream resolution.
	 */
	[[nodiscard]] cv::Size getEventResolution() const {
		cv::Size resolution;
		switch (discoveryResult.deviceType) {
			case CAER_DEVICE_DAVIS:
				resolution.width  = discoveryResult.deviceInfo.davisInfo.dvsSizeX;
				resolution.height = discoveryResult.deviceInfo.davisInfo.dvsSizeY;
				break;
			case CAER_DEVICE_DVS128:
				resolution.width  = discoveryResult.deviceInfo.dvs128Info.dvsSizeX;
				resolution.height = discoveryResult.deviceInfo.dvs128Info.dvsSizeY;
				break;
			case CAER_DEVICE_DVS132S:
				resolution.width  = discoveryResult.deviceInfo.dvs132sInfo.dvsSizeX;
				resolution.height = discoveryResult.deviceInfo.dvs132sInfo.dvsSizeY;
				break;
			case CAER_DEVICE_DVXPLORER:
				resolution.width  = discoveryResult.deviceInfo.dvXplorerInfo.dvsSizeX;
				resolution.height = discoveryResult.deviceInfo.dvXplorerInfo.dvsSizeY;
				break;
			default:
				throw std::runtime_error("Unsupported device type");
		}
		return resolution;
	}

	/**
	 * Retrieve frame stream resolution.
	 * @return 		Frame stream resolution or `std::nullopt` if the frame stream is not available.
	 */
	[[nodiscard]] std::optional<cv::Size> getFrameResolution() const {
		cv::Size resolution;
		switch (discoveryResult.deviceType) {
			case CAER_DEVICE_DAVIS: {
				resolution.width  = discoveryResult.deviceInfo.davisInfo.apsSizeX;
				resolution.height = discoveryResult.deviceInfo.davisInfo.apsSizeY;
				break;
			}
			default:
				return std::nullopt;
		}
		return resolution;
	}

	/**
	 * Check whether frame stream is available.
	 * @return 		True if frame stream is available, false otherwise.
	 */
	[[nodiscard]] bool isFrameStreamAvailable() const {
		// All supported cameras support event output right now
		return discoveryResult.deviceType == CAER_DEVICE_DAVIS;
	}

	/**
	 * Destructor: stops the readout thread.
	 */
	~CameraCapture() {
		device->dataStop();
	}

	/**
	 * Get camera name, which is a combination of the camera model and the serial number.
	 * @return 		String containing the camera model and serial number separated by an underscore character.
	 */
	[[nodiscard]] std::string getCameraName() const {
		return internal::getDiscoveredCameraName(discoveryResult);
	}

	/**
	 * Enable auto-exposure. To disable the auto-exposure, use the manual set exposure function.
	 * @return 			True if configuration was successful, false otherwise.
	 */
	bool enableDavisAutoExposure() {
		if (discoveryResult.deviceType != CAER_DEVICE_DAVIS) {
			return false;
		}

		deviceConfigSet(DAVIS_CONFIG_APS, DAVIS_CONFIG_APS_AUTOEXPOSURE, 1);
		return deviceConfigGet(DAVIS_CONFIG_APS, DAVIS_CONFIG_APS_AUTOEXPOSURE) == 1;
	}

	/**
	 * Disable auto-exposure and set a new fixed exposure value.
	 * @param exposure 	Exposure duration.
	 * @return 			True if configuration was successful, false otherwise.
	 */
	bool setDavisExposureDuration(const dv::Duration &exposure) {
		if (discoveryResult.deviceType != CAER_DEVICE_DAVIS) {
			return false;
		}

		auto exposureValue = static_cast<uint32_t>(exposure.count());
		deviceConfigSet(DAVIS_CONFIG_APS, DAVIS_CONFIG_APS_AUTOEXPOSURE, 0);
		deviceConfigSet(DAVIS_CONFIG_APS, DAVIS_CONFIG_APS_EXPOSURE, exposureValue);
		return deviceConfigGet(DAVIS_CONFIG_APS, DAVIS_CONFIG_APS_EXPOSURE) == exposureValue;
	}

	/**
	 * Set a new frame interval value. This interval defines the framerate output of the camera. The frames
	 * will be produced at the given interval, the interval can be reduced in case exposure time is longer
	 * than the frame interval.
	 * @param interval 	Output frame interval.
	 * @return 			True if configuration was successful, false otherwise.
	 */
	bool setDavisFrameInterval(const dv::Duration &interval) {
		if (discoveryResult.deviceType != CAER_DEVICE_DAVIS) {
			return false;
		}

		auto intervalValue = static_cast<uint32_t>(interval.count());
		deviceConfigSet(DAVIS_CONFIG_APS, DAVIS_CONFIG_APS_FRAME_INTERVAL, intervalValue);
		return deviceConfigGet(DAVIS_CONFIG_APS, DAVIS_CONFIG_APS_FRAME_INTERVAL) == intervalValue;
	}

	/**
	 * Get the configured frame interval.
	 * @return 			An optional containing the frame interval value, return `std::nullopt` in case frame
	 * 					interval setting is not available for the device.
	 */
	[[nodiscard]] std::optional<dv::Duration> getDavisFrameInterval() const {
		if (discoveryResult.deviceType != CAER_DEVICE_DAVIS) {
			return std::nullopt;
		}

		return dv::Duration(deviceConfigGet(DAVIS_CONFIG_APS, DAVIS_CONFIG_APS_FRAME_INTERVAL));
	}

	/**
	 * Get a configuration setting value from the connected device.
	 * @param moduleAddress		Module address. An integer number that represents a group of settings.
	 * @param parameterAddress	Parameter address. An integer number that specifies a parameter within a parameter
	 * 							module group.
	 * @return					Configured value of the parameter.
	 * @throws runtime_error	Exception is thrown if parameter is not available for the device.
	 */
	[[nodiscard]] uint32_t deviceConfigGet(const int8_t moduleAddress, const uint8_t parameterAddress) const {
		return device->configGet(moduleAddress, parameterAddress);
	}

	/**
	 * Set a configuration setting to a given value.
	 * @param moduleAddress		Module address. An integer number that represents a group of settings.
	 * @param parameterAddress	Parameter address. An integer number that specifies a parameter within a parameter
	 * 							module group.
	 * @param value				New value for the configuration.
	 * @throws runtime_error	Exception is thrown if parameter is not available for the device.
	 */
	void deviceConfigSet(const int8_t moduleAddress, const uint8_t parameterAddress, const uint32_t value) {
		device->configSet(moduleAddress, parameterAddress, value);
	}

	/**
	 * Set DVS chip bias sensitivity preset.
	 * @param sensitivity 	DVS sensitivity preset.
	 * @return 				True if configuration was successful, false otherwise.
	 */
	bool setDVSBiasSensitivity(const BiasSensitivity sensitivity) {
		if (discoveryResult.deviceType != CAER_DEVICE_DVXPLORER) {
			return false;
		}

		// Sensitivity settings are bit-exact with the enum
		deviceConfigSet(DVX_DVS_CHIP_BIAS, DVX_DVS_CHIP_BIAS_SIMPLE, static_cast<uint32_t>(sensitivity));
		return true;
	}

	/**
	 * Enable or disable DVS global hold setting.
	 * @param state		True to enable global hold, false to disable.
	 * @return 			True if configuration was successful, false otherwise.
	 */
	bool setDVSGlobalHold(const bool state) {
		if (discoveryResult.deviceType != CAER_DEVICE_DVXPLORER) {
			return false;
		}

		deviceConfigSet(DVX_DVS_CHIP, DVX_DVS_CHIP_GLOBAL_HOLD_ENABLE, state);
		return deviceConfigGet(DVX_DVS_CHIP, DVX_DVS_CHIP_GLOBAL_HOLD_ENABLE) == state;
	}

	/**
	 * Set davis data readout mode. The configuration will be performed if the connected camera is a DAVIS camera.
	 * @param mode 		New readout mode
	 * @return 			True if configuration was successful, false otherwise.
	 */
	bool setDavisReadoutMode(const DavisReadoutMode mode) {
		if (discoveryResult.deviceType != CAER_DEVICE_DAVIS) {
			return false;
		}

		bool runDVS = mode == DavisReadoutMode::EventsOnly || mode == DavisReadoutMode::EventsAndFrames;
		deviceConfigSet(DAVIS_CONFIG_DVS, DAVIS_CONFIG_DVS_RUN, runDVS);
		bool runAPS = mode == DavisReadoutMode::FramesOnly || mode == DavisReadoutMode::EventsAndFrames;
		deviceConfigSet(DAVIS_CONFIG_APS, DAVIS_CONFIG_APS_RUN, runAPS);

		return deviceConfigGet(DAVIS_CONFIG_DVS, DAVIS_CONFIG_DVS_RUN) == runDVS
			   && deviceConfigGet(DAVIS_CONFIG_APS, DAVIS_CONFIG_APS_RUN) == runAPS;
	}

	/**
	 * Set davis color mode. The configuration will be performed if the connected camera is a DAVIS camera.
	 * @param colorMode 	Color mode, either grayscale or color (if supported).
	 * @return 				True if configuration was successful, false otherwise.
	 */
	bool setDavisColorMode(const DavisColorMode colorMode) {
		if (discoveryResult.deviceType != CAER_DEVICE_DAVIS) {
			return false;
		}

		auto value = static_cast<uint32_t>(colorMode);
		deviceConfigSet(DAVIS_CONFIG_APS, DAVIS_CONFIG_APS_FRAME_MODE, value);
		return deviceConfigGet(DAVIS_CONFIG_APS, DAVIS_CONFIG_APS_FRAME_MODE) == value;
	}

	/**
	 * Read a packet from the camera and return a variant of any packet. You can use std::visit
	 * with `dv::io::DataReadHandler` to handle each type of packet using callback methods. This method
	 * might not maintain timestamp monotonicity between different stream types.
	 *
	 * @return			A variant containing data packet from the camera.
	 */
	[[nodiscard]] DataReadVariant readNext() {
		if (!keepRunning) {
			return DataReadHandler::OutputFlag::EndOfFile;
		}

		std::array<size_t, 4> indices{std::numeric_limits<size_t>::max(), std::numeric_limits<size_t>::max(),
			std::numeric_limits<size_t>::max(), std::numeric_limits<size_t>::max()};

		// Find the packet with lowest packet index
		if (!buffers.events.empty()) {
			indices[0] = buffers.events.front().first;
		}
		if (!buffers.triggers.empty()) {
			indices[1] = buffers.triggers.front().first;
		}
		if (!buffers.imu.empty()) {
			indices[2] = buffers.imu.front().first;
		}
		if (!buffers.frames.empty()) {
			indices[3] = buffers.frames.front().first;
		}

		const auto min = std::min_element(indices.begin(), indices.end());
		if (min == indices.end() || *min == std::numeric_limits<size_t>::max()) {
			return DataReadHandler::OutputFlag::Continue;
		}
		// Read the specified packet from the according packet buffer
		switch (std::distance(indices.begin(), min)) {
			case 0: {
				dv::EventStore events = convertEventsPacket(buffers.events.front().second);
				buffers.events.pop();
				return events;
			}
			case 1: {
				dv::cvector<dv::Trigger> triggers = convertTriggerPacket(buffers.triggers.front().second);
				buffers.triggers.pop();
				return triggers;
			}
			case 2: {
				dv::cvector<dv::IMU> imuData = convertImuPacket(buffers.imu.front().second);
				buffers.imu.pop();
				return imuData;
			}
			case 3: {
				dv::Frame frame = convertFramePacket(buffers.frames.front().second);
				buffers.frames.pop();
				return frame;
			}
			default:
				return DataReadHandler::OutputFlag::Continue;
		}
	}

	/**
	 * Read next packet from the camera and use a handler object to handle all types of packets. The function returns
	 * a true if end-of-file was not reached, so this function call can be used in a while loop like so:
	 * ```
	 * while (camera.handleNext(handler)) {
	 * 		// While-loop executes after each packet
	 * }
	 * ```
	 * @param handler	Handler instance that contains callback functions to handle different packets.
	 * @return			False to indicate end of data stream, true to continue.
	 */
	[[nodiscard]] bool handleNext(DataReadHandler &handler) {
		std::visit(handler, readNext());
		return !handler.eof;
	}

	/**
	 * Check whether camera is still connected.
	 * @return 		False if camera is disconnected, true if it is still connected and running.
	 */
	[[nodiscard]] bool isConnected() const {
		return keepRunning.load(std::memory_order_relaxed);
	}

	/**
	 * Checks whether the camera is a master camera in multiple camera setups. If camera does not have synchronization
	 * cable connected, it will identified as master camera.
	 * @return 		True if camera is master camera, false otherwise.
	 */
	[[nodiscard]] bool isMasterCamera() const {
		switch (discoveryResult.deviceType) {
			case CAER_DEVICE_DVXPLORER:
				return discoveryResult.deviceInfo.dvXplorerInfo.deviceIsMaster;
			case CAER_DEVICE_DVS132S:
				return discoveryResult.deviceInfo.dvs132sInfo.deviceIsMaster;
			case CAER_DEVICE_DVS128:
				return discoveryResult.deviceInfo.dvs128Info.deviceIsMaster;
			case CAER_DEVICE_DAVIS:
				return discoveryResult.deviceInfo.davisInfo.deviceIsMaster;
			default:
				throw dv::exceptions::InvalidArgument<int>("Unsupported device type", discoveryResult.deviceType);
		}
	}

	/**
	 * Get the configured IMU measurement rate. DVXplorer cameras support individual rates for accelerometer and
	 * gyroscope, in the case camera configured to have different rates, this function return the lowest value.
	 * @return IMU rate in Hz.
	 */
	[[nodiscard]] float getImuRate() const {
		switch (discoveryResult.deviceType) {
			case CAER_DEVICE_DVXPLORER: {
				auto accelRate = boschAccRateToFreq(deviceConfigGet(DVX_IMU, DVX_IMU_ACCEL_DATA_RATE));
				auto gyroRate  = boschGyroRateToFreq(deviceConfigGet(DVX_IMU, DVX_IMU_GYRO_DATA_RATE));
				return std::min(accelRate, gyroRate);
			}
			case CAER_DEVICE_DAVIS: {
				auto value = deviceConfigGet(DAVIS_CONFIG_IMU, DAVIS_CONFIG_IMU_SAMPLE_RATE_DIVIDER);
				return 1000.f / (1.f + static_cast<float>(value));
			}
			default:
				throw dv::exceptions::InvalidArgument<int>("Unsupported device type", discoveryResult.deviceType);
		}
	}

	/**
	 * Return pixel pitch distance for the connected camera model. The value is returned in meters, it is:
	 * - DVXplorer Lite - 18 micrometers (1.8e-5)
	 * - DVXplorer and DVXplorer Mini - 9 micrometers (9e-6)
	 * - DAVIS346 and DAVIS240 - 18.5 micrometers (1.85e-5)
	 * @return 	Pixel pitch distance in meters according to the connected device, returns `std::nullopt` if device
	 * 			can't be reliably identified.
	 */
	[[nodiscard]] std::optional<float> getPixelPitch() const noexcept {
		switch (discoveryResult.deviceType) {
			case CAER_DEVICE_DVXPLORER: {
				if (discoveryResult.deviceInfo.dvXplorerInfo.chipID == DVXPLORER_LITE_CHIP_ID) {
					return 18e-6f;
				}
				else if (discoveryResult.deviceInfo.dvXplorerInfo.chipID == DVXPLORER_CHIP_ID) {
					return 9e-6f;
				}
				else {
					return std::nullopt;
				}
			}
			case CAER_DEVICE_DAVIS: {
				return 18.5e-6f;
			}
			default:
				return std::nullopt;
		}
	}
};

} // namespace dv::io

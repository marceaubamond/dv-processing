#include <dv-processing/core/frame.hpp>
#include <dv-processing/io/mono_camera_recording.hpp>

#include <CLI/CLI.hpp>
#include <opencv2/highgui.hpp>

#include <thread>

int main(int ac, char **av) {
	std::string aedat4Path;

	CLI::App app{"Command-line aedat4 preview player of recorded frames"};

	app.add_option("-i,--input", aedat4Path, "Path to an input aedat4 file to be played.");
	try {
		app.parse(ac, av);
	}
	catch (const CLI::ParseError &e) {
		return app.exit(e);
	}

	// Construct the reader
	dv::io::MonoCameraRecording reader(aedat4Path);

	// Test whether the frame stream available
	if (!reader.isFrameStreamAvailable()) {
		throw dv::exceptions::InvalidArgument<dv::cstring>(
			"Aedat4 player only supports frame stream, the supplied file does not contain a frame stream", aedat4Path);
	}

	// Create a display window for the frames
	cv::namedWindow("AEDAT4 Player", cv::WINDOW_AUTOSIZE);

	// Buffer to store last frame
	dv::Frame lastFrame;
	lastFrame.timestamp = -1;

	// Create a handler instance for storing lambda callback functions for each incoming type
	dv::io::DataReadHandler handler;

	// Frame type handler
	handler.mFrameHandler = [&lastFrame](const dv::Frame &nextFrame) {
		// Buffer the first frame
		if (lastFrame.timestamp < 0) {
			lastFrame = nextFrame;
			return;
		}

		// Show the frame
		cv::imshow("AEDAT4 Player", lastFrame.image);

		// Sleep until next frame timestamp
		cv::waitKey(static_cast<int>((nextFrame.timestamp - lastFrame.timestamp) / 1000LL));

		// Move the next frame for rendering in next iteration
		lastFrame = nextFrame;
	};

	// Run the handler, this will block the execution of the main function until all data is read and parsed.
	// The appropriate handler callback function will be called on each incoming packet.
	reader.run(handler);

	return EXIT_SUCCESS;
}

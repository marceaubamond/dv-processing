#pragma once

#include <chrono>

namespace dv {

/**
 * Duration type that stores microsecond time period.
 */
using Duration = std::chrono::duration<int64_t, std::micro>;

} // namespace dv

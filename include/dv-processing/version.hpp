#pragma once

/**
 * dv-processing version (MAJOR * 10000 + MINOR * 100 + PATCH).
 */
#define DV_PROCESSING_VERSION_MAJOR 1
#define DV_PROCESSING_VERSION_MINOR 4
#define DV_PROCESSING_VERSION_PATCH 0
#define DV_PROCESSING_VERSION \
	((DV_PROCESSING_VERSION_MAJOR * 10000) + (DV_PROCESSING_VERSION_MINOR * 100) + DV_PROCESSING_VERSION_PATCH)

/**
 * dv-processing name string.
 */
#define DV_PROCESSING_NAME_STRING "dv-processing"

/**
 * dv-processing version string.
 */
#define DV_PROCESSING_VERSION_STRING "1.4.0"

#ifndef DV_PROCESSING_FEATURES_REDETECTION_STRATEGY_HPP
#define DV_PROCESSING_FEATURES_REDETECTION_STRATEGY_HPP

#include "tracker_base.hpp"

namespace dv::features {

/**
 * Implementation of different redection strategies for trackers.
 */
class RedetectionStrategy {
public:
	typedef std::shared_ptr<RedetectionStrategy> SharedPtr;
	typedef std::unique_ptr<RedetectionStrategy> UniquePtr;

	/**
	 * Decide the redetection of tracker features depending on the state of the tracker.
	 * @param tracker       Current state of the tracker.
	 * @return              True to perform redetection of features, false to continue.
	 */
	[[nodiscard]] virtual bool decideRedection(const dv::features::TrackerBase &tracker) = 0;

	virtual ~RedetectionStrategy() = default;
};

/**
 * No redetection strategy.
 */
class NoRedetection : public RedetectionStrategy {
public:
	/**
	 * Do not perform redetection.
	 * @return          Just return false always.
	 */
	bool decideRedection(const TrackerBase &) override {
		return false;
	}
};

/**
 * Redetection strategy based on number of features.
 */
class FeatureCountRedetection : public RedetectionStrategy {
protected:
	float numberOfFeatures = 0.5f;

public:
	/**
	 * Redetection strategy based on number of features.
	 * @param numberOfFeatures_     Feature count coefficient, redection is performed
	 *                              when feature count goes lower than the given proportion
	 *                              of maximum tracks, rederection will be executed.
	 */
	explicit FeatureCountRedetection(float numberOfFeatures_) : numberOfFeatures(numberOfFeatures_) {
	}

	/**
	 * Check whether to perform redetection.
	 * @param tracker       Current state of the tracker.
	 * @return              True to perform redetection of features, false to continue.
	 */
	[[nodiscard]] bool decideRedection(const TrackerBase &tracker) override {
		return static_cast<float>(tracker.getLastFrameResults()->keypoints.size())
			   < (static_cast<float>(tracker.getMaxTracks()) * numberOfFeatures);
	}
};

} // namespace dv::features

#endif // DV_PROCESSING_FEATURES_REDETECTION_STRATEGY_HPP

#ifndef DV_PROCESSING_CLUSTER_MEAN_SHIFT_HPP
#define DV_PROCESSING_CLUSTER_MEAN_SHIFT_HPP

#include "mean_shift/eigen_matrix_adaptor.hpp"
#include "mean_shift/event_store_adaptor.hpp"

#endif // DV_PROCESSING_CLUSTER_MEAN_SHIFT_HPP

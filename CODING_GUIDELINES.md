# DV Coding Guidelines

[[_TOC_]]

## C++ standard

We currently use the C++20 standard and support the following compilers and C++ standard libraries:

- GCC 10 and newer
- Clang 12 and newer
- GCC libstdc++ 10 and newer
- XCode 13 (Apple clang / libc++) and newer

We do not currently support Clang libc++ as its C++20 support is incomplete.

As such, the following C++20 features cannot be currently used:

- limited consteval support (https://en.cppreference.com/w/cpp/language/consteval)
- parenthesized initialization of aggregates (https://en.cppreference.com/w/cpp/language/aggregate_initialization)
- modules support (https://en.cppreference.com/w/cpp/language/modules)
- using enum (https://en.cppreference.com/w/cpp/language/enum#Using-enum-declaration)
- atomic for floats (https://en.cppreference.com/w/cpp/atomic/atomic#Specializations_for_floating-point_types)
- atomic std::shared_ptr (https://en.cppreference.com/w/cpp/memory/shared_ptr/atomic2)
- std::atomic_ref (https://en.cppreference.com/w/cpp/atomic/atomic_ref)
- synchronized buffered streams (https://en.cppreference.com/w/cpp/io/basic_osyncstream)
- calendar and timezone **(use https://github.com/HowardHinnant/date/ instead)**
- std::bit_cast (https://en.cppreference.com/w/cpp/numeric/bit_cast) **(use memcpy() workaround)**
- std::bind_front (https://en.cppreference.com/w/cpp/utility/functional/bind_front)
- ranges and their algorithms (https://en.cppreference.com/w/cpp/ranges) **(use old algorithms with begin() and end())**
- heterogeneous lookup for unordered containers **(use https://github.com/greg7mdp/parallel-hashmap/ instead)**
- std::assume_aligned() (https://en.cppreference.com/w/cpp/memory/assume_aligned)
- std::execution::unseq (https://en.cppreference.com/w/cpp/algorithm/execution_policy_tag)
- std::basic_stringbuf efficient access to underlying buffer via str()/view()
- std::format **(use fmt::format instead)**
- constexpr std::string **(can be emulated with fixed-size std::array)**
- constexpr std::vector **(can be emulated with fixed-size std::array)**
- std::stop_token and std::jthread **(continue using std::atomic_bool to stop threads)**
- atomic waiting and notifying, std::counting_semaphore, std::latch and std::barrier
- std::source_location **(use <dv-processing/exception/source_location.hpp> as alternative)**
- safe integral comparisons (https://en.cppreference.com/w/cpp/utility/intcmp)

## Code formatting

Always format your code using standard tools:

- C/C++/Java: clang-format
- Python: yapf
- CMake: cmake-format

## Class Structure

- Each class must be separated into `.cpp` and `.hpp` files.
- No local declarations in `.cpp` files.
    - Every symbol that is used in the `.cpp` file must be declared in the `.hpp` or files that are included.
- Declarations in the `.hpp` file should be ordered by accessibility modifer: `public`, `protected` and `private` (in
  this order).
- Each section above is then further divided in the following order:
    1. `static` variables
    1. `static` functions
    1. other variables
    1. constructors
    1. destructors
    1. other functions, in decreasing level of abstraction.
- Functions should be located above other functions that they depend on.
    - "Your code should read like an article" - Uncle Bob
      in *[Clean Code](https://www.oreilly.com/library/view/clean-code/9780136083238/)*
- All classes and structs follow either the Rule of Zero or the Rule of Five.
    - If any of {destructor, copy constructor/assignment or move constructor/assignment} needs to be implemented
      specifically, implement all of them.
        - This is the Rule of Five
        - Deleting ( = delete) is considered a specific implementation and requires the remaining methods to be
          implemented as well.
    - Most classes should not implement any of them (all defaulted and not user-provided).
        - This is the Rule of Zero

## General

- Use `auto` for local variable types wherever possible in C++ (`var` in Java).
- Don't use `(unsigned) int`, use fixed-width integers: `(u)int[8|16|32|64]_t`
- Make things `const` wherever they can be `const`.
    - If move constructors and move assignment operators need to be defined, member variables cannot be `const`, prefer
      the move operators over `const` members.
- Dead code is never allowed.
    - E.g. unreachable code, unused functions and variables
- Avoid the use of `goto`, there are only narrow use-cases, especially in older C code, where it turns out to be useful;
  exceptions and RAII in C++ provide much better lifetime and cleanup management.
- Try to fix as many warnings as possible.
    - Especially unused arguments often indicate an issue (either not used by mistake, or bad API).
    - Also integer comparisons of different sizes or sign can often be easily fixed by casting or using the appropriate
      unsigned types (`size_t`).
        - This indicates you've at least thought about the issue and what values your integers can take.
- Don't use arithmetic operations to perform boolean operations.
    - E.g. setting flags via addition `+` is not permitted, use `|` instead.
        - Addition only works if all flags are exact powers of two, which is not guaranteed (flags may imply other
          flags).
- Compare `std::string`s via the comparison operator
    - E.g. `if (str == "hello") {}`
- Check your grammar
    - All documentation and comments in English
    - Typos in code, comments, log messages or documentation should be avoided.
    - Most IDEs at least spell check comments and strings in English automatically or have plugins (
      QtCreator: https://github.com/CJCombrink/SpellChecker-Plugin/).

## Readability & Complexity

- Declare variables as locally as possible.
- All control blocks have braces around them.
    - `if () { ... }`, `while () { ... }`
    - One line statements without braces are forbidden.
- Instead of an `if (x) { ... }` that spans your entire 50-lines function, prefer
  `if (!x) { return; }` to return early.
- Avoid more than 5 levels of nested control blocks.
- Put parentheses `()` inside complex conditional statement expressions.
    - E.g. ```if (((a - b) > c) && (!d) && (e == f.x) && ((func(y) == 42) || (!fonc(z)))) {}```
- Newlines are your friend. Sprinkle them around to break up code blocks.
    - Really, they have no drawbacks!
- Use structs/classes to keep related data together.
- Keep your functions concise.
    - Nobody wants to read your 1000-liner wall of code, not even you yourself.

## Functions

- Pass objects of size > 16 bytes, as well as non-POD objects by reference.
    - Wherever possible, use references instead of pointers.
    - If pointers are required, use smart pointers wherever possible.
        - `std::unique_ptr`, `std::shared_ptr`, `std::weak_ptr`
    - Only use raw pointers wherever smart pointers cannot be used, mostly for old C API compatibility.
    - Whenever the object is not modified, pass it as `const`.
    - Use `std::string_view` wherever it makes sense.
- Use `[[nodiscard]]` wherever it makes sense.
    - Functions that do not modify their arguments nor have any other side-effects.
        - E.g. only returning a value, or computing a value and then returning it but not storing it.
    - Functions where it is a logical error to discard the return value.

## Header Includes

- Only include what you use.
- Avoid includes in header files wherever possible.
- Prefer `#pragma once` to header guards.

## Error handling

- Functions must not return `null` or error codes, exceptions must be thrown in case of errors.
- Error handling code should be separated from code for normal processing (no nested `if`/`else` statements checking for
  and dealing with errors)

## Comments

- Code comments should use the `//` style.
    - Long multi-line comments (4+ lines) can use the `/* */` style
- Prefer abstraction to comments.
    - If you find yourself writing gigantic comments to explain your code, this code should most likely be wrapped into
      a function with a useful name and appropriate documentation.

## File Handling

- C++ widely uses the RAII (Resource Acquisition Is Initialization) paradigm, so generally objects clean up after
  themselves when they go out of scope and their destructor is called.
    - Calling `file.close()` or similar is often not needed, especially for temporary objects.
- When loading data from file, or network, always check, check, check!
    - Never assume data is present or makes sense.
    - OpenCV for example silently returns empty/zero objects when using cv::FileStorage
        - To actually know if there were values in the loaded file, you have to compare the returned object's `.type()`
          to `cv::FileNode::NONE`.

## Naming Conventions

### Functions

- Functions shall follow camelCase, starting with a lowercase letter.
    - Regex: `^[a-z][a-zA-Z0-9]*$`
- Function names should be pronounceable, meaningful, and generally longer than four characters.

### Variables

- Class and struct member names shall start with an 'm' followed by a CamelCase name.
    - This makes autocompletion inside classes far easier.
    - Regex: `^m[A-Z][a-zA-Z0-9]*$`
- Local variables and function parameters shall follow camelCase, starting with a lowercase letter.
    - Regex: `^[a-z][a-zA-Z0-9]*$`
- Variable names should be pronounceable, meaningful, and generally longer than two characters (except for loop
  variables of course). Make sure that all class member variables are actually used somewhere. The compiler can't always
  analyze this.

### Classes, Structs, Unions and Enum Types

- All types shall follow CamelCase naming.
    - Regex: `^[A-Z][a-zA-Z0-9]*$`

### Macros, Constants, Enum Values and Template Type Names

- Avoid macros wherever possible
    - Really, don't do it.
- Macros and constants (constexpr, static) at global or class level shall be named ALL_UPPERCASE.
    - Regex: `^[A-Z][A-Z0-9_]*$`

### Namespaces

- Namespaces shall be all_lowercase.
    - Regex: `^[a-z][a-z0-9_]*$`

### Template Parameters

- Template parameters shall be ALL_UPPERCASE.
    - Regex: `^[A-Z][A-Z0-9_]*$`

### Files

- `C++` files: `.cpp` and `.hpp`
- `C` files: `.c` and `.h`
- Files should have useful names, containing their purpose. Always add a proper file extension.
- File names should consist entirely of the a-z A-Z 0-9 - and _ characters to be truly portable.
- Dots are generally also allowed, though usage of only one to separate the extension is preferred.
- Source code files should follow the lowercase snake_case format.
    - Regex: `^[a-z0-9_]+\.(c|cpp|h|hpp)$`

A simple C++ regex to sanitize a file name:

```
#include <regex>
const std::regex filenameCleanupRegex{"[^a-zA-Z-_\\d]"};
auto cleanFilename = std::regex_replace(originalFilename, filenameCleanupRegex, "_");
```

## Libraries

- file operations: std::filesystem (IMPORTANT: use this instead of boost::filesystem)
- better error handling: dv::exceptions
- printf-style printing in C++: fmt::format
- networking: boost::asio
- string splitting on tokens (eg. CSV): boost::tokenizer
- simple string split/join: boost::algorithm::string (prefer fmt::format for joining)

## Documentation

Public interfaces should be documented using Doxygen-compatible comments syntax:

- https://github.com/stan-dev/stan/wiki/How-to-Write-Doxygen-Doc-Comments
- http://www.doxygen.nl/index.html

## DV SDK specific

- `dv::Frame::FrameFormat` is OpenCV-compatible, the values behind the enum correspond to OpenCV frame format values.
- `dv::cvector` (from <dv-processing/data/cvector.hpp>) provides a nice std::vector alternative, that is low-level C-compatible
  across multiple compilers and standard libraries, can convert to/from std::vector, and adds some useful features like
  append() and addition of vectors.

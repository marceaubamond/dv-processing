# dv-processing

Generic algorithms for event cameras.

# Dependencies:

Linux, MacOS X or Windows <br />
gcc >= 10.0 or clang >= 12 or Apple clang >= 13 <br />
libstdc++ >= 10.0 or Apple libc++ >= 13 <br />
cmake >= 3.16 <br />
Boost >= 1.74 <br />
OpenCV >= 4.2.0 <br />
Eigen >= 3.3.9 <br />
libcaer >= 3.3.12 <br />
fmt >= 7.1.3 <br />
lz4 <br />
zstd <br />
Optional: libbacktrace (for better stack traces on error) <br />

# API Documentation

The API documentation is available in HTML format, please open `docs/index.html` with your browser to access the
documentation.

## Install dependencies on Ubuntu 20.04

```bash
sudo add-apt-repository ppa:inivation-ppa/inivation
sudo apt-get update
sudo apt-get install git gcc-10 g++-10 cmake boost-inivation libopencv-dev libeigen3-dev libcaer-dev libfmt-dev liblz4-dev libzstd-dev
```

## Install dependencies on Ubuntu 18.04

```bash
sudo add-apt-repository ppa:ubuntu-toolchain-r/test
sudo add-apt-repository ppa:inivation-ppa/inivation-bionic
sudo apt-get update
sudo apt-get install git gcc-10 g++-10 cmake boost-inivation libopencv-dev libeigen3-dev libcaer-dev libfmt-dev liblz4-dev libzstd-dev
```

# Installation

The use of library is possible using two approaches - system installation or as a git submodule.

## System wide installation

1. Clone the repository:

```bash
git clone git@gitlab.com:inivation/dv/dv-processing.git
cd dv-processing
```

2. Build and verify the library using unit tests:

```bash
mkdir build && cd build
CC=gcc-10 CXX=g++-10 cmake -DCMAKE_INSTALL_PREFIX=/usr ..
make -j4 -s
make test
```

3. Install the headers:

```bash
sudo make install
```

4. Use in your cmake projects:

```cmake
FIND_PACKAGE(dv-processing REQUIRED)

# link your targets against the library
TARGET_LINK_LIBRARIES(your_target
	dv::processing
	...)
```

## Git submodule usage

1. Add the repository as a submodule in your project:

```bash
git submodule add git@gitlab.com:inivation/dv/dv-processing.git path/for/dv-processing
```

2. Use in your cmake project:

```cmake
ADD_SUBDIRECTORY(path/for/dv-processing)

# link your targets against the library
TARGET_LINK_LIBRARIES(your_target
	dv::processing
	...)
```

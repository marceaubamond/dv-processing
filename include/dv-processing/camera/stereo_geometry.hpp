#pragma once

#include "../data/depth_frame_base.hpp"
#include "calibrations/camera_calibration.hpp"
#include "camera_geometry.hpp"

#include <opencv2/imgproc.hpp>

namespace dv::camera {

/**
 * A class that performs stereo geometry operations and rectification of a stereo camera.
 */
class StereoGeometry {
protected:
	cv::Mat mLeftRemap1;
	cv::Mat mLeftRemap2;
	cv::Mat mRightRemap1;
	cv::Mat mRightRemap2;
	cv::Mat mLeftProjection;
	cv::Mat mRightProjection;

	std::vector<uint8_t> mLeftValidMask;
	std::vector<uint8_t> mRightValidMask;

	std::vector<cv::Point2i> mLeftRemapLUT;
	std::vector<cv::Point2i> mRightRemapLUT;

	cv::Size mLeftResolution;
	cv::Size mRightResolution;

	std::vector<float> mDistLeft;
	DistortionModel mLeftDistModel;
	std::vector<float> mDistRight;
	DistortionModel mRightDistModel;

	std::optional<float> mPixelPitch = std::nullopt;

	float mBaseline;

	static std::vector<cv::Point2f> initCoordinateList(const cv::Size &resolution) {
		std::vector<cv::Point2f> coordinates;
		// Populate undistort events input map with all possible (x, y) address combinations.
		for (int y = 0; y < resolution.height; y++) {
			for (int x = 0; x < resolution.width; x++) {
				// Use center of pixel to get better approximation, since we're using floats anyway.
				coordinates.emplace_back(static_cast<float>(x), static_cast<float>(y));
			}
		}
		return coordinates;
	}

	static dv::EventStore remapEventsInternal(const dv::EventStore &events, const cv::Size &resolution,
		const std::vector<uint8_t> &mask, const std::vector<cv::Point2i> &remapLUT) {
		dv::EventStore output;
		for (const auto &event : events) {
			const auto pos = static_cast<size_t>(event.y()) * static_cast<size_t>(resolution.width)
							 + static_cast<size_t>(event.x());
			dv::runtime_assert(pos < mask.size(), "Event coordinates are out of range");
			if (mask[pos] == 1) {
				const cv::Point2i &coords = remapLUT[pos];
				output.emplace_back(event.timestamp(), coords.x, coords.y, event.polarity());
			}
		}
		return output;
	}

	static void createLUTs(const cv::Size &resolution, const cv::Matx33f &cameraMatrix, const cv::Mat &distortion,
		const cv::Mat &R, const cv::Mat &P, std::vector<uint8_t> &outputMask,
		std::vector<cv::Point2i> &outputRemapLUT) {
		std::vector<cv::Point2f> undistortEventInputMap, undistortEventOutputMap;

		undistortEventInputMap = initCoordinateList(resolution);
		cv::undistortPoints(undistortEventInputMap, undistortEventOutputMap, cameraMatrix, distortion, R, P);
		std::copy(undistortEventOutputMap.begin(), undistortEventOutputMap.end(), std::back_inserter(outputRemapLUT));

		for (size_t i = 0; i < outputRemapLUT.size(); ++i) {
			cv::Point2i coord = outputRemapLUT[i];
			if (coord.x < 0 || coord.y < 0 || coord.x >= resolution.width || coord.y >= resolution.height) {
				outputMask[i] = 0;
			}
		}
	}

public:
	using UniquePtr = std::unique_ptr<StereoGeometry>;
	using SharedPtr = std::shared_ptr<StereoGeometry>;

	/**
	 * Position enum for a single camera in a stereo configuration.
	 */
	enum class CameraPosition { Left, Right };

	/**
	 * Initialize a stereo geometry class using two camera geometries for each of the stereo camera pair and a
	 * transformation matrix that describes the transformation from right camera to the left.
	 * @param leftCamera		Left camera geometry.
	 * @param rightCamera		Right camera geometry.
	 * @param transformToLeft	A vector containing a homogenous transformation from right to the left camera.
	 * 							Vector should contain exactly 16 numbers (as per 4x4 homogenous transformation matrix)
	 * 							in a row-major ordering.
	 */
	StereoGeometry(const CameraGeometry &leftCamera, const CameraGeometry &rightCamera,
		const std::vector<float> &transformToLeft) :
		mLeftResolution(leftCamera.getResolution()),
		mRightResolution(rightCamera.getResolution()),
		mLeftDistModel(leftCamera.getDistortionModel()),
		mRightDistModel(rightCamera.getDistortionModel()) {
		cv::Mat RN[2], Q;

		const cv::Mat transform
			= cv::Mat(cv::Size(4, 4), CV_32FC1, const_cast<void *>(static_cast<const void *>(transformToLeft.data())));
		cv::Mat R = transform(cv::Rect(0, 0, 3, 3));
		cv::Mat T = transform(cv::Rect(3, 0, 1, 3));
		mBaseline = T.at<float>(0, 0);
		const cv::Mat camLeft(leftCamera.getCameraMatrix());
		R.convertTo(R, CV_64FC1);
		T.convertTo(T, CV_64FC1);
		const cv::Mat camRight(rightCamera.getCameraMatrix());
		mDistLeft  = leftCamera.getDistortion();
		mDistRight = rightCamera.getDistortion();
		const cv::Mat distLeftMat(mDistLeft.size(), 1, CV_32FC1, mDistLeft.data());
		const cv::Mat distRightMat(mDistRight.size(), 1, CV_32FC1, mDistRight.data());

		cv::stereoRectify(camLeft, distLeftMat, camRight, distRightMat, mLeftResolution, R, T, RN[0], RN[1],
			mLeftProjection, mRightProjection, Q, cv::CALIB_ZERO_DISPARITY);

		cv::initUndistortRectifyMap(leftCamera.getCameraMatrix(), leftCamera.getDistortion(), RN[0], mLeftProjection,
			leftCamera.getResolution(), CV_16SC2, mLeftRemap1, mLeftRemap2);
		cv::initUndistortRectifyMap(rightCamera.getCameraMatrix(), rightCamera.getDistortion(), RN[1], mRightProjection,
			rightCamera.getResolution(), CV_16SC2, mRightRemap1, mRightRemap2);

		mLeftValidMask.resize(static_cast<size_t>(mLeftResolution.area()), 1);
		mRightValidMask.resize(static_cast<size_t>(mRightResolution.area()), 1);

		createLUTs(mLeftResolution, leftCamera.getCameraMatrix(), distLeftMat, RN[0], mLeftProjection, mLeftValidMask,
			mLeftRemapLUT);
		createLUTs(mRightResolution, rightCamera.getCameraMatrix(), distRightMat, RN[1], mRightProjection,
			mRightValidMask, mRightRemapLUT);
	}

	/**
	 * Create a stereo geometry class from left and right camera calibration instances.
	 * @param leftCalibration 	Left camera calibration.
	 * @param rightCalibration	Right camera calibration.
	 */
	StereoGeometry(const calibrations::CameraCalibration &leftCalibration,
		const calibrations::CameraCalibration &rightCalibration) :
		StereoGeometry(leftCalibration.getCameraGeometry(), rightCalibration.getCameraGeometry(),
			rightCalibration.transformationToC0) {
		if (leftCalibration.metadata->pixelPitch.has_value()) {
			mPixelPitch = *leftCalibration.metadata->pixelPitch;
		}
	}

	/**
	 * Apply remapping to an input image to rectify it.
	 * @param cameraPosition 	Indication whether image is from left or right camera.
	 * @param image 			Input image.
	 * @return 					Rectified image.
	 */
	[[nodiscard]] cv::Mat remapImage(const CameraPosition cameraPosition, const cv::Mat &image) const {
		cv::Mat output;
		switch (cameraPosition) {
			case CameraPosition::Left:
				cv::remap(image, output, mLeftRemap1, mLeftRemap2, cv::INTER_CUBIC, cv::BORDER_CONSTANT);
				break;
			case CameraPosition::Right:
				cv::remap(image, output, mRightRemap1, mRightRemap2, cv::INTER_CUBIC, cv::BORDER_CONSTANT);
				break;
		}
		return output;
	}

	/**
	 * Apply remapping on input events.
	 * @param cameraPosition 	Indication whether image is from left or right camera.
	 * @param events 			Input events.
	 * @return 					Event with rectified coordinates.
	 */
	[[nodiscard]] dv::EventStore remapEvents(const CameraPosition cameraPosition, const dv::EventStore &events) const {
		switch (cameraPosition) {
			case CameraPosition::Left:
				return remapEventsInternal(events, mLeftResolution, mLeftValidMask, mLeftRemapLUT);
			case CameraPosition::Right:
				return remapEventsInternal(events, mRightResolution, mRightValidMask, mRightRemapLUT);
		}
	}

	/**
	 * Retrieve left camera geometry class that can project coordinates into stereo rectified space.
	 * @return 		Camera geometry instance.
	 */
	[[nodiscard]] dv::camera::CameraGeometry getLeftCameraGeometry() const {
		return {mDistLeft, mLeftProjection.at<float>(0, 0), mLeftProjection.at<float>(1, 1),
			mLeftProjection.at<float>(0, 2), mLeftProjection.at<float>(1, 2), mLeftResolution, mLeftDistModel};
	}

	/**
	 * Retrieve right camera geometry class that can project coordinates into stereo rectified space.
	 * @return 		Camera geometry instance.
	 */
	[[nodiscard]] dv::camera::CameraGeometry getRightCameraGeometry() const {
		return {mDistRight, mRightProjection.at<float>(0, 0), mRightProjection.at<float>(1, 1),
			mRightProjection.at<float>(0, 2), mRightProjection.at<float>(1, 2), mRightResolution, mRightDistModel};
	}

	/**
	 * Estimate depth given the disparity map and a list of events. The coordinates will be rectified and
	 * a disparity value will be looked up in the disparity map. The depth of each event is calculated
	 * using an equation: depth = (focalLength * baseline) / (disparity * pixelPitch). Focal length is expressed
	 * in meter distance.
	 *
	 * The function requires knowledge about the pixel pitch distance which needs to be provided prior to
	 * calculations. The pixel pitch can be available in the camera calibration (in this case it will be looked up
	 * during construction of the class). If the pixel pitch is not available there, it must be provided manually
	 * using the `setPixelPitch` method. The pixel pitch value can be looked up in `dv::io::CameraCapture` class
	 * in case if running the stereo estimation in a live camera scenario.
	 *
	 * For practical applications, depth estimation should be evaluated prior to any use. The directly estimated depth
	 * values can contain measurable errors which should be accounted for - the errors can usually be within 10-20%
	 * fixed absolute error distance. Usually this comes from various inaccuracies and can be mitigated by introducing
	 * a correction factor for the depth estimate.
	 * @param disparity			Disparity map.
	 * @param events			Input events.
	 * @param disparityScale	Scale of disparity value in the disparity map, if subpixel accuracy is enabled
	 * 							in the block matching, this value will be equal to 16.
	 * @return					A depth event store, the events will contain the same information as in the
	 * 							input, but additionally will have the depth value. Events whose coordinates
	 * 							are outside of image bounds after rectification will be skipped.
	 */
	[[nodiscard]] dv::DepthEventStore estimateDepth(
		const cv::Mat &disparity, const dv::EventStore &events, const float disparityScale = 16.f) const {
		DepthEventStore output;

		if (!mPixelPitch.has_value()) {
			throw dv::exceptions::RuntimeError("Depth estimation requires known pixel pitch distance, please set "
											   "this value using `setPixelPitch` method before calling this method.");
		}

		const float focalLength = static_cast<float>(mLeftProjection.at<double>(0, 0)) * mPixelPitch.value();

		const float baselineFocal = std::abs(focalLength * mBaseline);
		for (const auto &event : events) {
			auto pos = static_cast<const size_t>(event.y()) * static_cast<size_t>(mLeftResolution.width)
					   + static_cast<size_t>(event.x());
			if (mLeftValidMask[pos]) {
				const cv::Point2i &pt = mLeftRemapLUT[pos];
				int16_t rawDisparity  = disparity.at<int16_t>(pt);
				if (rawDisparity <= 0) {
					continue;
				}
				const float disp  = (static_cast<float>(rawDisparity) / disparityScale) * mPixelPitch.value();
				const float depth = baselineFocal / disp;
				output.emplace_back(event.timestamp(), event.x(), event.y(), event.polarity(),
					static_cast<uint16_t>(std::round(depth * 1000.f)));
			}
		}

		return output;
	}

	/**
	 * Convert a disparity map into a depth frame. Each disparity value is converted into depth using the equation
	 * depth = (focalLength * baseline) / (disparity * pixelPitch). Output frame contains distance values expressed
	 * in integer values of millimeter distance.
	 *
	 * NOTE: Output depth frame will not have a timestamp value, it is up to the user of this method to set
	 * correct timestamp of the disparity map.
	 * @param disparity 		Input disparity map.
	 * @param disparityScale	Scale of disparity value in the disparity map, if subpixel accuracy is enabled
	 * 						in the block matching, this value will be equal to 16.
	 * @return 					A converted depth frame.
	 */
	[[nodiscard]] dv::DepthFrame toDepthFrame(const cv::Mat &disparity, const float disparityScale = 16.f) const {
		if (!mPixelPitch.has_value()) {
			throw dv::exceptions::RuntimeError("Depth estimation requires known pixel pitch distance, please set "
											   "this value using `setPixelPitch` method before calling this method.");
		}

		dv::DepthFrame dFrame;
		dFrame.sizeX = static_cast<int16_t>(disparity.cols);
		dFrame.sizeY = static_cast<int16_t>(disparity.rows);
		dFrame.depth.resize(static_cast<size_t>(dFrame.sizeX * dFrame.sizeY));

		cv::Mat depthFrame(dFrame.sizeY, dFrame.sizeX, CV_16SC1, dFrame.depth.data());

		cv::Mat floatDisparity;
		disparity.convertTo(floatDisparity, CV_32FC1, 1.0 / static_cast<double>(disparityScale));
		const auto pitch = static_cast<double>(mPixelPitch.value());

		const double focalLength   = mLeftProjection.at<double>(0, 0) * pitch;
		const double baselineFocal = std::abs(focalLength * static_cast<double>(mBaseline));

		floatDisparity = (baselineFocal / (floatDisparity * pitch));
		floatDisparity.convertTo(depthFrame, CV_16SC1, 1000.);

		dv::runtime_assert(dFrame.depth.size() == static_cast<size_t>(disparity.size().area()),
			"Output depth frame size doesn't match input disparity map size");

		return dFrame;
	}

	/**
	 * Get the configured pixel pitch distance (in meters).
	 * @return 	Pixel pitch distance in meters, `std::nullopt` if value is unknown.
	 */
	[[nodiscard]] const std::optional<float> &getPixelPitch() const {
		return mPixelPitch;
	}

	/**
	 * Set a pixel pitch distance value. This value must be expressed in meters.
	 * @param pixelPitch 	New pixel pitch distance value in meters.
	 */
	void setPixelPitch(const float pixelPitch) {
		mPixelPitch = pixelPitch;
	}
};

} // namespace dv::camera

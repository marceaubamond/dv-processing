#ifndef DV_PROCESSING_EXCEPTION_EXCEPTIONS_TYPE_EXCEPTIONS_HPP
#define DV_PROCESSING_EXCEPTION_EXCEPTIONS_TYPE_EXCEPTIONS_HPP

#include "../../data/cstring.hpp"
#include "../exception_base.hpp"

namespace dv::exceptions {

namespace info {
struct TypeError {
	using Info = dv::cstring;

	static std::string format(const Info &info) {
		return fmt::format("Type: {:s})", info);
	}
};
} // namespace info
using TypeError = Exception_<info::TypeError>;

} // namespace dv::exceptions

#endif // DV_PROCESSING_EXCEPTION_EXCEPTIONS_TYPE_EXCEPTIONS_HPP

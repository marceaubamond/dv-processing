#pragma once

#include "../exception/exceptions/generic_exceptions.hpp"

#include <libcaercpp/devices/device_discover.hpp>

namespace dv::io {

namespace internal {

[[nodiscard]] static inline std::string chipIDToName(const int16_t chipID) {
	switch (chipID) {
		case 0:
			return "DAVIS240A";
		case 1:
			return "DAVIS240B";
		case 2:
			return "DAVIS240C";
		case 3:
			return "DAVIS128";
		case 5: // DAVIS346B -> only FSI chip.
			return "DAVIS346";
		case 6:
			return "DAVIS640";
		case 7:
			return "DAVIS640H";
		case 8: // PixelParade.
			return "DAVIS208";
		case 9: // DAVIS346Cbsi -> only BSI chip.
			return "DAVIS346BSI";
		default:
			throw dv::exceptions::InvalidArgument<int16_t>("Unsupported chip id received", chipID);
	}
}

[[nodiscard]] static inline std::string getDiscoveredCameraName(const struct caer_device_discovery_result &discovery) {
	switch (discovery.deviceType) {
		case CAER_DEVICE_DAVIS: {
			return chipIDToName(discovery.deviceInfo.davisInfo.chipID)
				.append("_")
				.append(discovery.deviceInfo.davisInfo.deviceSerialNumber);
		}
		case CAER_DEVICE_DVS128: {
			return std::string("DVS128_").append(discovery.deviceInfo.dvs128Info.deviceSerialNumber);
		}
		case CAER_DEVICE_DVS132S: {
			return std::string("DVS132S_").append(discovery.deviceInfo.dvs132sInfo.deviceSerialNumber);
		}
		case CAER_DEVICE_DVXPLORER: {
			return std::string("DVXplorer_").append(discovery.deviceInfo.dvXplorerInfo.deviceSerialNumber);
		}
		default:
			throw dv::exceptions::InvalidArgument<uint16_t>("Unsupported device type", discovery.deviceType);
	}
}

} // namespace internal

/**
 * Retrieve a list of connected cameras. The list will contain camera names, which are supported
 * for `dv::CameraCapture` class.
 * @return	A list of currently connected camera names.
 * @throws InvalidArgument 	Exception is thrown if discovered device reports type or chip id that is not supported.
 */
[[nodiscard]] static inline std::vector<std::string> discoverDevices() {
	std::vector<std::string> names;
	const auto allCameras = libcaer::devices::discover::all();
	for (const caer_device_discovery_result &camera : allCameras) {
		names.push_back(internal::getDiscoveredCameraName(camera));
	}
	return names;
}

} // namespace dv::io

#ifndef DV_PROCESSING_CORE_FRAME_HPP
#define DV_PROCESSING_CORE_FRAME_HPP

#include "core.hpp"

namespace dv {

/**
 * An accumulator base that can be used to implement different types of accumulators.
 * Two provided implementations are the `dv::Accumulator` which is highly configurable
 * and provides numerous ways of generating a frame from events. Another implementation
 * is the `dv::PixelAccumulator` which accumulates event in a histogram representation
 * with configurable contribution, but it is more efficient compared to generic
 * accumulator since it uses 8-bit unsigned integers as internal memory type.
 */
class AccumulatorBase {
protected:
	// output
	cv::Size shape_;

public:
	typedef std::shared_ptr<AccumulatorBase> SharedPtr;
	typedef std::unique_ptr<AccumulatorBase> UniquePtr;

	/**
	 * Accumulator constructor from known event camera sensor dimensions.
	 * @param shape 	Sensor dimensions
	 */
	explicit AccumulatorBase(const cv::Size &shape) : shape_(shape) {
	}

	/**
	 * Accumulate given event store packet into a frame.
	 * @param packet 	Event packet to be accumulated.
	 */
	virtual void accumulate(const EventStore &packet) = 0;

	/**
	 * Generate a frame from previously accumulated events. The function should
	 * replace existing data with new data and types that are required by the accumulator.
	 *
	 *
	 * @param outFrame 	Output frame, containing accumulated image.
	 */
	virtual void generateFrame(dv::Frame &outFrame) = 0;

	/**
	 * Get the image dimensions expected by the accumulator.
	 * @return 			Image dimensions
	 */
	[[nodiscard]] const cv::Size &getShape() const {
		return shape_;
	}

	/**
	 * Generates the accumulation frame (potential surface) at the time of the
	 * last consumed event.
	 * The function returns an OpenCV frame to work
	 * with.
	 * @return An OpenCV frame containing the accumulated potential surface.
	 */
	dv::Frame generateFrame() {
		dv::Frame out;
		generateFrame(out);
		return out;
	}

	/**
	 * Output stream operator support for frame generation/
	 * @param mat 	Output image
	 * @return 		Self
	 */
	dv::Frame &operator>>(dv::Frame &mat) {
		generateFrame(mat);
		return mat;
	}

	/**
	 * Accumulate the given packet.
	 * @param packet 	Input event packet.
	 */
	inline void accept(const EventStore &packet) {
		accumulate(packet);
	}

	virtual ~AccumulatorBase() = default;
};

/**
 * Common accumulator class that allows to accumulate events into a frame.
 * The class is highly configurable to adapt to various use cases. This
 * is the preferred functionality for projecting events onto a frame.
 *
 * Accumulation of the events is performed on a floating point frame,
 * with every event contributing a fixed amount to the potential. Timestamps
 * of the last contributions are stored as well, to allow for a decay.
 *
 * Due to performance, no check on the event coordinates inside image plane is performed,
 * unless compiled specifically in DEBUG mode.
 * Events out of the image plane bounds will result in undefined behaviour, or program
 * termination in DEBUG mode.
 */
class Accumulator : public AccumulatorBase {
public:
	/**
	 * Decay function to be used to decay the surface potential.
	 *
	 * * `NONE`: Do not decay at all. The potential can be reset manually
	 *    by calling the `clear` function
	 *
	 * * `LINEAR`: Perform a linear decay with  given slope. The linear decay goes
	 *    from currentpotential until the potential reaches the neutral potential
	 *
	 * * `EXPONENTIAL`: Exponential decay with time factor tau. The potential
	 *    eventually converges to zero.
	 *
	 * * `STEP`: Decay sharply to neutral potential after the given time.
	 *    Constant potential before.
	 */
	enum class Decay { NONE = 0, LINEAR = 1, EXPONENTIAL = 2, STEP = 3 };

private:
	// input
	bool rectifyPolarity_    = false;
	float eventContribution_ = .0;
	float maxPotential_      = .0;
	float neutralPotential_  = .0;
	float minPotential_      = .0;

	// decay
	Decay decayFunction_   = Decay::NONE;
	double decayParam_     = .0;
	bool synchronousDecay_ = false;

	// state
	TimeSurface decayTimeSurface_;
	cv::Mat potentialSurface_;
	int64_t highestTime_ = 0;
	int64_t lowestTime_  = -1;
	bool resetTimestamp  = true;

	// internal use methods
	/**
	 * __INTERNAL_USE_ONLY__
	 * Decays the potential at coordinates x, y to the given time, respecting the
	 * decay function. Updates the time surface to the last decay.
	 * @param x The x coordinate of the value to be decayed
	 * @param y The y coordinate of the value to be decayed
	 * @param time The time to which the value should be decayed to.
	 */
	void decay(int16_t x, int16_t y, int64_t time) {
		// normal handling for all the other functions
		int64_t lastDecayTime = decayTimeSurface_(y, x);
		dv::runtime_assert(lastDecayTime <= time, "last decay time bigger than current time, time going backwards!");

		const float lastPotential = potentialSurface_.at<float>(y, x);
		switch (decayFunction_) {
			case Decay::LINEAR: {
				potentialSurface_.at<float>(y, x)
					= (lastPotential >= neutralPotential_)
						  ? std::max(lastPotential
										 - static_cast<float>(static_cast<double>(time - lastDecayTime) * decayParam_),
							  neutralPotential_)
						  : std::min(lastPotential
										 + static_cast<float>(static_cast<double>(time - lastDecayTime) * decayParam_),
							  neutralPotential_);
				decayTimeSurface_(y, x) = time;
				break;
			}

			case Decay::EXPONENTIAL: {
				potentialSurface_.at<float>(y, x)
					= ((lastPotential - neutralPotential_)
						  * static_cast<float>(
							  expf(-(static_cast<float>(time - lastDecayTime)) / static_cast<float>(decayParam_))))
					  + neutralPotential_;
				decayTimeSurface_(y, x) = time;
				break;
			}

			case Decay::STEP:
				// STEP decay is handled at frame generation time.
			case Decay::NONE:
			default: {
				break;
			}
		}
	}

	/**
	 * __INTERNAL_USE_ONLY__
	 * Contributes the effect of a single event onto the potential surface.
	 * @param x The x coordinate of where to contribute to
	 * @param y The y coordinate of where to contribute to
	 * @param polarity The polarity of the contribution
	 */
	void contribute(int16_t x, int16_t y, bool polarity) {
		const float lastPotential = potentialSurface_.at<float>(y, x);
		float contribution        = eventContribution_;
		if (!rectifyPolarity_ && !polarity) {
			contribution = -contribution;
		}

		float newPotential = std::min(std::max(lastPotential + contribution, minPotential_), maxPotential_);
		potentialSurface_.at<float>(y, x) = newPotential;
	}

public:
	/**
	 * Silly default constructor. This generates an accumulator with zero size.
	 * An accumulator with zero size does not work. This constructor just exists
	 * to make it possible to default initialize an Accumulator to later redefine.
	 */
	Accumulator() : AccumulatorBase(cv::Size(0, 0)) {
	}

	/**
	 * Accumulator constructor
	 * Creates a new Accumulator with the given params. By selecting the params
	 * the right way, the Accumulator can be used for a multitude of applications.
	 * The class also provides static factory functions that adjust the parameters
	 * for common use cases.
	 *
	 * @param resolution The size of the resulting frame. This must be at least the
	 * dimensions of the eventstream supposed to be added to the accumulator,
	 * otherwise this will result in memory errors.
	 * @param decayFunction The decay function to be used in this accumulator.
	 * The decay function is one of `NONE`, `LINEAR`, `EXPONENTIAL`, `STEP`. The
	 * function behave like their mathematical definitions, with LINEAR AND STEP
	 * going back to the `neutralPotential` over time, EXPONENTIAL going back to 0.
	 * @param decayParam The parameter to tune the decay function. The parameter has
	 * a different meaning depending on the decay function chosen:
	 * `NONE`: The parameter is ignored
	 * `LINEAR`: The paramaeter describes the (negative) slope of the linear function
	 * `EXPONENTIAL`: The parameter describes tau, by which the time difference is divided.
	 * @param synchronousDecay if set to true, all pixel values get decayed to the same time
	 * as soon as the frame is generated. If set to false, pixel values remain at the state
	 * they had when the last contribution came in.
	 * @param eventContribution The contribution a single event has onto the potential
	 * surface. This value gets interpreted positively or negatively depending on the
	 * event polarity
	 * @param maxPotential The upper cut-off value at which the potential surface
	 * is clipped
	 * @param neutralPotential The potential the decay function converges to over time.
	 * @param minPotential The lower cut-off value at which the potential surface
	 * is clipped
	 * @param rectifyPolarity Describes if the polarity of the events should be kept
	 * or ignored. If set to true, all events behave like positive events.
	 */
	explicit Accumulator(const cv::Size &resolution, Accumulator::Decay decayFunction = Decay::EXPONENTIAL,
		double decayParam = 1.0e+6, bool synchronousDecay = false, float eventContribution = 0.15f,
		float maxPotential = 1.0f, float neutralPotential = 0.f, float minPotential = 0.f,
		bool rectifyPolarity = false) :
		AccumulatorBase(resolution),
		rectifyPolarity_(rectifyPolarity),
		eventContribution_(eventContribution),
		maxPotential_(maxPotential),
		neutralPotential_(neutralPotential),
		minPotential_(minPotential),
		decayFunction_(decayFunction),
		decayParam_(decayParam),
		synchronousDecay_(synchronousDecay),
		decayTimeSurface_(TimeSurface(resolution)),
		potentialSurface_(cv::Mat(resolution, CV_32F, static_cast<double>(neutralPotential))),
		highestTime_(0) {
	}

	/**
	 * Accumulates all the events in the supplied packet and puts them onto the
	 * accumulation surface.
	 * @param packet The packet containing the events that should be
	 * accumulated.
	 */
	void accumulate(const EventStore &packet) override {
		if (potentialSurface_.empty()) {
			return;
		}

		if (packet.isEmpty()) {
			return;
		}

		if ((decayFunction_ == Decay::NONE) || (decayFunction_ == Decay::STEP)) {
			// for step and none, only contribute
			for (const Event &event : packet) {
				dv::runtime_assert(0 <= event.y() && event.y() <= shape_.height, "event Y coordinate out of bounds");
				dv::runtime_assert(0 <= event.x() && event.x() <= shape_.width, "event X coordinate out of bounds");

				contribute(event.x(), event.y(), event.polarity());
			}
		}
		else {
			// for all others, decay before contributing
			for (const Event &event : packet) {
				dv::runtime_assert(0 <= event.y() && event.y() <= shape_.height, "event Y coordinate out of bounds");
				dv::runtime_assert(0 <= event.x() && event.x() <= shape_.width, "event X coordinate out of bounds");

				decay(event.x(), event.y(), event.timestamp());
				contribute(event.x(), event.y(), event.polarity());
			}
		}

		if (resetTimestamp) {
			lowestTime_    = packet.getLowestTime();
			resetTimestamp = false;
		}
		highestTime_ = packet.getHighestTime();
	}

	/**
	 * Generates the accumulation frame (potential surface) at the time of the
	 * last consumed event.
	 * The function writes the output image into the given `image` argument.
	 * The output frame will contain data with type CV_8U.
	 * @deprecated Please use the generateFrame functions which uses dv::Frame input, it will handle the timing as well.
	 * @param image The image to copy the data to
	 */
	[[deprecated("Please use the new generateFrame API that use dv::Frame as input.")]] void generateFrame(
		cv::Mat &image) {
		// Just wrap up the cv::Mat in a dv::Frame, the changes will happen in place anyway
		dv::Frame frame(0, image);
		generateFrame(frame);
	}

	/**
	 * Generates the accumulation frame (potential surface) at the time of the
	 * last consumed event.
	 * The function writes the output image into the given `frame` argument.
	 * The output frame will contain data with type CV_8U.
	 * @param frame the frame to copy the data to
	 */
	void generateFrame(dv::Frame &frame) override {
		cv::Mat &image = frame.image;

		// Validate the image
		if (image.empty()) {
			image = cv::Mat(shape_, CV_8UC1);
		}
		else if ((image.size() != shape_) || (image.channels() != 1) || (image.type() != CV_8UC1)) {
			throw std::invalid_argument(
				"Accumulator requires a single-channel image with pixel type CV_8U and exact dimensions");
		}

		if (synchronousDecay_ && (decayFunction_ != Decay::NONE) && (decayFunction_ != Decay::STEP)) {
			for (int y = 0; y < shape_.height; y++) {
				for (int x = 0; x < shape_.width; x++) {
					decay(static_cast<int16_t>(x), static_cast<int16_t>(y), highestTime_);
				}
			}
		}

		// Normalize min-max
		const double scaleFactor = 255.0 / static_cast<double>(maxPotential_ - minPotential_);
		const double shiftFactor = -static_cast<double>(minPotential_) * scaleFactor;
		potentialSurface_.convertTo(image, CV_8UC1, scaleFactor, shiftFactor);

		// Filling in the frame datastruct
		frame          = dv::Frame(lowestTime_, image);
		frame.exposure = dv::Duration(highestTime_ - frame.timestamp);
		frame.source   = dv::FrameSource::ACCUMULATION;

		// in case of step decay function, clear potential surface
		if (decayFunction_ == Decay::STEP) {
			potentialSurface_.setTo(static_cast<double>(neutralPotential_));
			lowestTime_ = -1;
		}

		resetTimestamp = true;
	}

	/**
	 * Clears the potential surface by setting it to the neutral value.
	 * This function does not reset the time surface.
	 */
	void clear() {
		potentialSurface_ = cv::Mat(shape_, CV_32F, static_cast<double>(neutralPotential_));
		lowestTime_       = -1;
	}

	// setters
	/**
	 * If set to true, all events will incur a positive contribution to the
	 * potential surface
	 * @param rectifyPolarity The new value to set
	 */
	void setRectifyPolarity(bool rectifyPolarity) {
		Accumulator::rectifyPolarity_ = rectifyPolarity;
	}

	/**
	 * Contribution to the potential surface an event shall incur.
	 * This contribution is either counted positively (for positive events
	 * or when `rectifyPolatity` is set).
	 * @param eventContribution The contribution a single event shall incur
	 */
	void setEventContribution(float eventContribution) {
		Accumulator::eventContribution_ = eventContribution;
	}

	/**
	 * @param maxPotential the max potential at which the surface should be capped at
	 */
	void setMaxPotential(float maxPotential) {
		Accumulator::maxPotential_ = maxPotential;
	}

	/**
	 * Set a new neutral potential value. This will also reset the cached potential surface
	 * to the given new value.
	 * @param neutralPotential The neutral potential to which the decay function should go.
	 * Exponential decay always goes to 0. The parameter is ignored there.
	 */
	void setNeutralPotential(float neutralPotential) {
		Accumulator::neutralPotential_ = neutralPotential;
		potentialSurface_              = cv::Mat(shape_, CV_32F, static_cast<double>(neutralPotential_));
	}

	/**
	 * @param minPotential the min potential at which the surface should be capped at
	 */
	void setMinPotential(float minPotential) {
		Accumulator::minPotential_ = minPotential;
	}

	/**
	 * @param decayFunction The decay function the module should use to perform the decay
	 */
	void setDecayFunction(Decay decayFunction) {
		Accumulator::decayFunction_ = decayFunction;
	}

	/**
	 * The decay param. This is slope for linear decay, tau for exponential decay
	 * @param decayParam The param to be used
	 */
	void setDecayParam(double decayParam) {
		Accumulator::decayParam_ = decayParam;
	}

	/**
	 * If set to true, all valued get decayed to the frame generation time at
	 * frame generation. If set to false, the values only get decayed on activity.
	 * @param synchronousDecay the new value for synchronoues decay
	 */
	void setSynchronousDecay(bool synchronousDecay) {
		Accumulator::synchronousDecay_ = synchronousDecay;
	}

	[[nodiscard]] bool isRectifyPolarity() const {
		return rectifyPolarity_;
	}

	[[nodiscard]] float getEventContribution() const {
		return eventContribution_;
	}

	[[nodiscard]] float getMaxPotential() const {
		return maxPotential_;
	}

	[[nodiscard]] float getNeutralPotential() const {
		return neutralPotential_;
	}

	[[nodiscard]] float getMinPotential() const {
		return minPotential_;
	}

	[[nodiscard]] Decay getDecayFunction() const {
		return decayFunction_;
	}

	[[nodiscard]] double getDecayParam() const {
		return decayParam_;
	}

	using AccumulatorBase::generateFrame;

	/**
	 * Accumulates the event store into the accumulator.
	 * @param store The event store to be accumulated.
	 * @return A reference to this Accumulator.
	 */
	Accumulator &operator<<(const EventStore &store) {
		accumulate(store);
		return *this;
	}

	/**
	 * Retrieved a copy of the currently accumulated potential surface. Potential surface contains raw
	 * floating point values aggregated by the accumulator, the values are within the configured range of
	 * [minPotential; maxPotential]. This returns a deep copy of the potential surface.
	 * @return 	Potential surface image containing CV_32FC1 data.
	 */
	[[nodiscard]] cv::Mat getPotentialSurface() const {
		return potentialSurface_.clone();
	}
};

static_assert(dv::concepts::FrameOutputGenerator<Accumulator>);
static_assert(dv::concepts::EventToFrameConverter<Accumulator, dv::EventStore>);

/**
 * `dv::PixelAccumulator` accumulates events in a histogram representation
 * with configurable contribution, but it is more efficient compared to generic
 * accumulator since it uses 8-bit unsigned integers as internal memory type.
 *
 * The PixelAccumulator behaves the same as a generic `dv::Accumulator` with STEP
 * decay function, neutral and minimum value of 0.0, maximum value of 1.0 and
 * configurable event contribution. The difference is that it doesn't use floating
 * point numbers for the potential surface representation. The output data type of
 * this accumulator is single channel 8-bit unsigned integer (CV_8UC1). Accumulation
 * is performed using integer operations as well.
 * Due to performance, no check on the event coordinates inside image plane is performed,
 * unless compiled specifically in DEBUG mode.
 * Events out of the image plane bounds will result in undefined behaviour, or program
 * termination in DEBUG mode.
 */
class PixelAccumulator : public AccumulatorBase {
protected:
	/**
	 * Buffer to keep the latest events
	 */
	dv::EventStore buffer;

	/**
	 * Max unsigned byte value
	 */
	uint8_t maxByteValue = 255;

	/**
	 * Default contribution
	 */
	float contribution = 0.25f;
	/**
	 * Increment value for a single event
	 */
	uint8_t drawIncrement = (static_cast<uint8_t>(static_cast<float>(maxByteValue) * contribution));

	/**
	 * A look-up table for increment values at each possible pixel value.
	 */
	std::vector<uint8_t> incrementLUT;

	bool ignorePolarity = true;

	float neutralValue = 0.f;

	uint8_t neutralByteValue = 0;

	float decay = -1.0;

	std::vector<uint8_t> decayLUT;

	cv::Mat imageBuffer;

public:
	/**
	 * Create a pixel accumulator with known image dimensions.
	 * @param resolution 	Dimensions of the expected event sensor
	 */
	explicit PixelAccumulator(const cv::Size &resolution) : PixelAccumulator(resolution, 0.25f, true, 0.f, -1.f) {
	}

	/**
	 * Create a pixel accumulator with known image dimensions and event contribution.
	 * @param resolution 		Dimensions of the expected event sensor
	 * @param contribution_ 	Contribution coefficient for a single event. The contribution value is multiplied
	 * 							by the maximum possible pixel value (255) to get the increment value.
	 * 							E.g. contribution value of 0.1 will increment a pixel value at a single event
	 * 							coordinates by 26.
	 * @param ignorePolarity_	Set ignore polarity option. All events are considered positive if enabled.
	 * @param neutralValue_		Neutral potential value. Neutral value is the default pixel value when decay is
	 * 							disabled and the value that pixels decay into when decay is enabled. The range for
	 * 							neutral potential value is [0.0; 1.0], where 1.0 stands for maximum possible
	 * 							potential - 255 in 8-bit pixel representation.
	 * @param decay_			Decay value. Non-negative values enable the decay which then applies a fixed
	 * 							decay to pixels that are not neutral to decay into the neutral value over time.
	 * 							If enabled, decay is applied after each frame generation. The range for
	 * 							decay value is [0.0; 1.0], where 1.0 stands for maximum possible
	 * 							potential - 255 in 8-bit pixel representation.
	 */
	PixelAccumulator(const cv::Size &resolution, const float contribution_, const bool ignorePolarity_ = true,
		const float neutralValue_ = 0.f, const float decay_ = -1.f) :
		AccumulatorBase(resolution), ignorePolarity(ignorePolarity_) {
		setContribution(contribution_);
		setNeutralValue(neutralValue_);
		setDecay(decay_);
	}

	/**
	 * Get the contribution coefficient for a single event. The contribution value is multiplied
	 * by the maximum possible pixel value (255) to get the increment value.
	 * E.g. contribution value of 0.1 will increment a pixel value at a single event
	 * coordinates by 26.
	 * @return		Contribution coefficient
	 */
	[[nodiscard]] float getContribution() const {
		return contribution;
	}

	/**
	 * Set new contribution coefficient.
	 * @param contribution_ 	Contribution coefficient for a single event. The contribution value is multiplied
	 * 							by the maximum possible pixel value (255) to get the increment value.
	 * 							E.g. contribution value of 0.1 will increment a pixel value at a single event
	 * 							coordinates by 26.
	 */
	void setContribution(const float contribution_) {
		if (contribution_ < 0.f || contribution_ > 1.f) {
			throw std::invalid_argument("Contribution value should be in the range [0.0; 1.0]");
		}
		contribution  = contribution_;
		drawIncrement = static_cast<uint8_t>(std::ceil(static_cast<float>(maxByteValue) * contribution_));

		incrementLUT.clear();

		if (!ignorePolarity) {
			// LUT for decrement
			for (int i = 0; i <= maxByteValue; i++) {
				incrementLUT.push_back(static_cast<uint8_t>(std::max(i - drawIncrement, 0)));
			}
		}

		// LUT for increments
		for (int i = 0; i <= maxByteValue; i++) {
			incrementLUT.push_back(static_cast<uint8_t>(std::min<int32_t>(i + drawIncrement, maxByteValue)));
		}
	}

	/**
	 * Perform accumulation on given events.
	 * @param packet 	Event store containing event to be accumulated.
	 */
	void accumulate(const EventStore &packet) override {
		buffer.add(packet);
	}

	/**
	 * Generates the accumulation frame (potential surface) at the time of the
	 * last consumed event.
	 * The function writes the output image into the given `outFrame` argument.
	 * The output frame will contain data with type CV_8UC1.
	 *
	 * The function resets any events accumulated up to this function call.
	 * @param frame the frame to generate the image to
	 */
	void generateFrame(dv::Frame &frame) override {
		cv::Mat image;
		const bool performDecay = !decayLUT.empty();
		if (performDecay) {
			// Do with decay, use the buffered image
			image = imageBuffer;
		}
		else {
			// Avoid decay
			image = frame.image;
		}

		if (image.empty()) {
			image = cv::Mat(shape_, CV_8UC1, cv::Scalar(neutralByteValue));
		}
		else if ((image.size() != shape_) || (image.channels() != 1) || (image.type() != CV_8UC1)) {
			throw std::invalid_argument(
				"PixelAccumulator requires a single-channel image with pixel type CV_8U and exact dimensions");
		}
		else if (!performDecay) {
			image = neutralByteValue;
		}

		if (ignorePolarity) {
			for (const dv::concepts::AddressableEvent auto &event : buffer) {
				dv::runtime_assert(0 <= event.y() && event.y() <= shape_.height, "event Y coordinate out of bounds");
				dv::runtime_assert(0 <= event.x() && event.x() <= shape_.width, "event X coordinate out of bounds");

				auto &imgVal = image.at<uint8_t>(event.y(), event.x());
				imgVal       = static_cast<uint8_t>(incrementLUT[imgVal]);
			}
		}
		else {
			for (const dv::concepts::AddressableEvent auto &event : buffer) {
				dv::runtime_assert(0 <= event.y() && event.y() <= shape_.height, "event Y coordinate out of bounds");
				dv::runtime_assert(0 <= event.x() && event.x() <= shape_.width, "event X coordinate out of bounds");

				auto &imgVal = image.at<uint8_t>(event.y(), event.x());
				imgVal = static_cast<uint8_t>(incrementLUT[imgVal + (static_cast<size_t>(event.polarity()) * 256)]);
			}
		}

		const int64_t frameTimestamp = (buffer.isEmpty() ? -1 : buffer.getLowestTime());
		if (performDecay) {
			// Apply fixed decay
			cv::LUT(image, decayLUT, imageBuffer);
			frame = dv::Frame(frameTimestamp, imageBuffer.clone());
		}
		else {
			frame = dv::Frame(frameTimestamp, image);
		}

		frame.exposure = buffer.duration();
		frame.source   = dv::FrameSource::ACCUMULATION;

		// Clear the buffer
		buffer = EventStore();
	}

	/**
	 * Clear the buffered events.
	 */
	void reset() {
		buffer = EventStore();
		if (!imageBuffer.empty()) {
			imageBuffer = cv::Mat();
		}
	}

	/**
	 * Accumulates the event store into the accumulator.
	 * @param store The event store to be accumulated.
	 * @return A reference to this PixelAccumulator.
	 */
	PixelAccumulator &operator<<(const EventStore &store) {
		accumulate(store);
		return *this;
	}

	/**
	 * Check whether ignore polarity option is set to true.
	 * @return True if the accumulator assumes all events as positive, false otherwise.
	 */
	[[nodiscard]] bool isIgnorePolarity() const {
		return ignorePolarity;
	}

	/**
	 * Set ignore polarity option. All events are considered positive if enabled.
	 * @param ignorePolarity_ True to enable ignore polarity option.
	 */
	void setIgnorePolarity(const bool ignorePolarity_) {
		PixelAccumulator::ignorePolarity = ignorePolarity_;
		// Regenerate LUT
		setContribution(contribution);
	}

	/**
	 * Get the neutral potential value for the accumulator. The range for potential value is
	 * [0.0; 1.0], where 1.0 stands for maximum possible potential - 255 in 8-bit pixel representation.
	 * @return Neutral potential value in range [0.0; 1.0]
	 */
	[[nodiscard]] float getNeutralValue() const {
		return neutralValue;
	}

	/**
	 * Set the neutral potential value. The value should be in range 0.0 to 1.0, other values will be clamped
	 * to this range.
	 * @param neutralValue_ Neutral potential value in range [0.0; 1.0].
	 */
	void setNeutralValue(const float neutralValue_) {
		neutralValue = std::clamp(neutralValue_, 0.f, 1.f);
		neutralByteValue
			= static_cast<uint8_t>(std::clamp<int>(neutralValue * static_cast<float>(maxByteValue), 0, maxByteValue));
	}

	/**
	 * Get current decay value. Negative values represent disabled decay.
	 * @return Decay value.
	 */
	[[nodiscard]] float getDecay() const {
		return decay;
	}

	/**
	 * Set the decay value. A negative value will assume the decay as disabled. Any value above 1.0 will be clamped to
	 * one.
	 * @param decay_ Decay value. Negative value disabled the decay.
	 */
	void setDecay(const float decay_) {
		decay = decay_;
		decayLUT.clear();

		if (decay < 0.f) {
			return;
		}

		auto decayByteValue = static_cast<uint8_t>(static_cast<float>(maxByteValue) * std::min(decay, 1.f));

		for (int32_t i = 0; i <= maxByteValue; i++) {
			if (i == neutralByteValue) {
				// value is neutral
				decayLUT.push_back(neutralByteValue);
			}
			else if (i < neutralByteValue) {
				// Value is less than neutral, increase back to neutral
				decayLUT.push_back(static_cast<uint8_t>(std::clamp<int32_t>(i + decayByteValue, 0, neutralByteValue)));
			}
			else {
				// Value is more than neutral, decrease back to neutral
				decayLUT.push_back(
					static_cast<uint8_t>(std::clamp<int32_t>(i - decayByteValue, neutralByteValue, maxByteValue)));
			}
		}
	}

	using AccumulatorBase::generateFrame;
};

static_assert(dv::concepts::EventToFrameConverter<PixelAccumulator, dv::EventStore>);

} // namespace dv

#endif // DV_PROCESSING_CORE_FRAME_HPP
